package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.container.Flowers;
import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.GrowingTips;
import com.epam.rd.java.basic.task8.entity.VisualParameters;
import com.epam.rd.java.basic.task8.tags.FlowerTags;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.logging.Logger;

import static com.epam.rd.java.basic.task8.tags.FlowerTags.*;

/**
 * Controller for DOM parser.
 */
public class DOMController {

    private static final Logger LOGGER = Logger.getLogger(DOMController.class.getName());
    private String xmlFileName;
    private DocumentBuilder documentBuilder;
    private Flowers flowers;

    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
        flowers = new Flowers();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            documentBuilder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            LOGGER.severe(e.getMessage());
        }
    }

    public void parse() {
        Document document;
        try {
            document = documentBuilder.parse(xmlFileName);
            Element root = document.getDocumentElement();
            NodeList nodes = root.getElementsByTagName(FLOWER.getValue());
            for (int i = 0; i < nodes.getLength(); i++) {
                Element element = (Element) nodes.item(i);
                Flower flower = buildFlower(element);
                flowers.add(flower);
            }

        } catch (SAXException | IOException e) {
            LOGGER.severe(e.getMessage());
        }
    }

    public Flower buildFlower(Element element) {
        Flower flower = new Flower();
        flower.setName(getTextContent(element, NAME.getValue()));
        flower.setSoil(getTextContent(element, SOIL.getValue()));
        flower.setOrigin(getTextContent(element, ORIGIN.getValue()));
        flower.setMultiplying(getTextContent(element, MULTIPLYING.getValue()));
        Element visualParameters = (Element) element.getElementsByTagName(VISUALPARAMETERS.getValue()).item(0);
        flower.setVisualParameters(buildVisualParameters(visualParameters));
        Element growingTips = (Element) element.getElementsByTagName(GROWINGTIPS.getValue()).item(0);
        flower.setGrowingTips(buildGrowingTips(growingTips));

        return flower;
    }

    private GrowingTips buildGrowingTips(Element element) {
        GrowingTips growingTips = new GrowingTips();
        growingTips.setTempreture(Integer.parseInt(getTextContent(element, TEMPERATURE.getValue())));
        growingTips.setLighting(getTextContent(element, LIGHTING.getValue()));
        growingTips.setWatering(Integer.parseInt(getTextContent(element, WATERING.getValue())));

        return growingTips;
    }

    private VisualParameters buildVisualParameters(Element element) {
        VisualParameters visualParameters = new VisualParameters();
        visualParameters.setStemColour(getTextContent(element, STEMCOLOUR.getValue()));
        visualParameters.setLeafColour(getTextContent(element, LEAFCOLOUR.getValue()));
        visualParameters.setAveLenFlower(Integer.parseInt(getTextContent(element, AVELENFLOWER.getValue())));

        return visualParameters;
    }

    private String getTextContent(Element element, String value) {
        Node node = element.getElementsByTagName(value).item(0);
        return node.getTextContent();
    }

    public Flowers getFlowers() {
        return flowers;
    }
}



















