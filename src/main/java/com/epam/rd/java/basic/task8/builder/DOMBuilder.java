package com.epam.rd.java.basic.task8.builder;

import com.epam.rd.java.basic.task8.container.Flowers;
import com.epam.rd.java.basic.task8.entity.Flower;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.logging.Logger;

import static com.epam.rd.java.basic.task8.tags.FlowerTags.*;

public class DOMBuilder extends XMLBuilder {
    private static final Logger LOGGER = Logger.getLogger(DOMBuilder.class.getName());

    public DOMBuilder(Flowers flowers, String outputXmlFile) {
        super(flowers, outputXmlFile);
    }

    @Override
    public void buildXML() {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder documentBuilder = factory.newDocumentBuilder();
            Document doc = documentBuilder.newDocument();
            Element root = doc.createElement(FLOWERS.getValue());
            root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            root.setAttribute("xmlns", "http://www.nure.ua");
            root.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd");
            doc.appendChild(root);

            for (Flower flower : flowers) {
                Element entity = doc.createElement(FLOWER.getValue());
                //
                Element name = doc.createElement(NAME.getValue());
                name.appendChild(doc.createTextNode(flower.getName()));

                Element soil = doc.createElement(SOIL.getValue());
                soil.appendChild(doc.createTextNode(flower.getSoil()));

                Element origin = doc.createElement(ORIGIN.getValue());
                origin.appendChild(doc.createTextNode(flower.getOrigin()));

                Element visualParameters = doc.createElement(VISUALPARAMETERS.getValue());
                //
                Element steamColour = doc.createElement(STEMCOLOUR.getValue());
                steamColour.appendChild(doc.createTextNode(flower.getVisualParameters().getStemColour()));

                Element leafColour = doc.createElement(LEAFCOLOUR.getValue());
                leafColour.appendChild(doc.createTextNode(flower.getVisualParameters().getLeafColour()));

                Element aveLenFlower = doc.createElement(AVELENFLOWER.getValue());
                aveLenFlower.setAttribute("measure", "cm");
                aveLenFlower.appendChild(doc.createTextNode(String.valueOf(flower.getVisualParameters().getAveLenFlower())));

                Element growingTips = doc.createElement(GROWINGTIPS.getValue());
                //
                Element temperature = doc.createElement(TEMPERATURE.getValue());
                temperature.setAttribute("measure","celcius");
                temperature.appendChild(doc.createTextNode(String.valueOf(flower.getGrowingTips().getTempreture())));

                Element lighting = doc.createElement(LIGHTING.getValue());
                lighting.setAttribute("lightRequiring","yes");
                lighting.appendChild(doc.createTextNode(flower.getGrowingTips().getLighting()));

                Element watering = doc.createElement(WATERING.getValue());
                watering.setAttribute("measure","mlPerWeek");
                watering.appendChild(doc.createTextNode(String.valueOf(flower.getGrowingTips().getWatering())));

                Element multiplying = doc.createElement(MULTIPLYING.getValue());
                multiplying.appendChild(doc.createTextNode(flower.getMultiplying()));

                visualParameters.appendChild(steamColour);
                visualParameters.appendChild(leafColour);
                visualParameters.appendChild(aveLenFlower);

                growingTips.appendChild(temperature);
                growingTips.appendChild(lighting);
                growingTips.appendChild(watering);

                entity.appendChild(name);
                entity.appendChild(soil);
                entity.appendChild(origin);
                entity.appendChild(visualParameters);
                entity.appendChild(growingTips);
                entity.appendChild(multiplying);

                root.appendChild(entity);

                write(doc);
            }

        } catch (ParserConfigurationException |FileNotFoundException |TransformerException e){
            LOGGER.severe(e.getMessage());

        }
    }

    private void write(Document doc) throws TransformerException, FileNotFoundException {
        TransformerFactory factory = TransformerFactory.newInstance();
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
        Transformer tr = factory.newTransformer();
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(new FileOutputStream(outputXmlFile));
        tr.transform(source, result);
    }
}
