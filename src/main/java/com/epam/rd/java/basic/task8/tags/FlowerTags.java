package com.epam.rd.java.basic.task8.tags;

public enum FlowerTags {
    FLOWERS("flowers"),
    FLOWER("flower"),
    NAME("name"),
    SOIL("soil"),
    ORIGIN("origin"),
    STEMCOLOUR("stemColour"),
    LEAFCOLOUR("leafColour"),
    AVELENFLOWER("aveLenFlower"),
    TEMPERATURE("temperature"),
    LIGHTING("lighting"),
    WATERING("watering"),
    MULTIPLYING("multiplying"),
    VISUALPARAMETERS("visualParameters"),
    GROWINGTIPS("growingTips");


    private String value;

    FlowerTags(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
