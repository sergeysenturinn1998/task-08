package com.epam.rd.java.basic.task8.entity;

import java.util.Objects;

public class GrowingTips {
    private int tempreture;     // температура (от 0 и выше по Цельсию)
    private String lighting;    // освещение (yes/no)
    private int watering;       // кл-во воды (мл в неделю)

    public GrowingTips(){}

    public  GrowingTips(String lighting, int tempreture, int watering){
        this.lighting=lighting;
        this.tempreture=tempreture;
        this.watering=watering;
    }

    public int getTempreture() {
        return tempreture;
    }

    public void setTempreture(int tempreture) {
        this.tempreture = tempreture;
    }

    public String getLighting() {
        return lighting;
    }

    public void setLighting(String lighting) {
        this.lighting = lighting;
    }

    public int getWatering() {
        return watering;
    }

    public void setWatering(int watering) {
        this.watering = watering;
    }

    @Override
    public String toString() {
        return "GrowingTips{" +
                "tempreture=" + tempreture +
                ", lighting='" + lighting + '\'' +
                ", watering=" + watering +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GrowingTips that = (GrowingTips) o;
        return tempreture == that.tempreture && watering == that.watering && Objects.equals(lighting, that.lighting);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tempreture, lighting, watering);
    }
}
