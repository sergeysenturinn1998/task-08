package com.epam.rd.java.basic.task8.container;

import com.epam.rd.java.basic.task8.entity.Flower;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Flowers implements Iterable<Flower> {
    private final List<Flower> container;

    public Flowers() {
        container = new ArrayList<>();
    }

    public List<Flower> getFlowersContainer() {
        return container;
    }

    public void add(Flower flower){
        container.add(flower);
    }

    @Override
    public Iterator<Flower> iterator() {
        return container.iterator();
    }

    @Override
    public String toString() {
        return "Flowers{" +
                "container=" + container +
                '}';
    }
}
