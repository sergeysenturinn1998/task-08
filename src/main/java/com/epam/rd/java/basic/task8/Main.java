package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.builder.DOMBuilder;
import com.epam.rd.java.basic.task8.builder.SAXBuilder;
import com.epam.rd.java.basic.task8.builder.StAXBuilder;
import com.epam.rd.java.basic.task8.container.Flowers;
import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entity.Flower;

import java.util.Comparator;

public class Main {

    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            return;
        }

        String xmlFileName = args[0];
        System.out.println("Input ==> " + xmlFileName);

        ////////////////////////////////////////////////////////
        // DOM
        ////////////////////////////////////////////////////////

        // get container
        DOMController domController = new DOMController(xmlFileName);
        // PLACE YOUR CODE HERE
        domController.parse();
        Flowers flowersDom = domController.getFlowers();

        // sort (case 1)
        // PLACE YOUR CODE HERE
        flowersDom.getFlowersContainer().sort(Comparator.comparing(Flower::getName));

        // save
        String outputXmlFile = "output.dom.xml";
        // PLACE YOUR CODE HERE
        DOMBuilder domBuilder = new DOMBuilder(flowersDom, outputXmlFile);
        domBuilder.buildXML();

        ////////////////////////////////////////////////////////
        // SAX
        ////////////////////////////////////////////////////////

        // get
        SAXController saxController = new SAXController(xmlFileName);
        saxController.parse();
        Flowers flowersSAX = saxController.getFlowers();


        // sort  (case 2)
        flowersSAX.getFlowersContainer().sort(Comparator.comparing(Flower::getSoil));

        // save
        outputXmlFile = "output.sax.xml";

        SAXBuilder saxBuilder = new SAXBuilder(flowersSAX,outputXmlFile);
        saxBuilder.buildXML();

		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////

		// get
		STAXController staxController = new STAXController(xmlFileName);
		staxController.parse();
        Flowers flowersStAX = staxController.getFlowers();

		// sort  (case 3)
		flowersStAX.getFlowersContainer().sort(Comparator.comparing(Flower::getOrigin));

		// save
		outputXmlFile = "output.stax.xml";
        StAXBuilder stAXBuilder= new StAXBuilder(flowersStAX,outputXmlFile);
        stAXBuilder.buildXML();
    }

}
