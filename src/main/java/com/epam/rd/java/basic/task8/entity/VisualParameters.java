package com.epam.rd.java.basic.task8.entity;

import java.util.Objects;

public class VisualParameters {
    private String stemColour;  // цвет стебля
    private String leafColour;  // цвет листьев
    private int aveLenFlower;   // средняя длинна цветка (см)

    public VisualParameters() {
    }

    public VisualParameters(String stemColour, String leafColour, int aveLenFlower) {
        this.stemColour = stemColour;
        this.leafColour = leafColour;
        this.aveLenFlower = aveLenFlower;
    }

    public String getStemColour() {
        return stemColour;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public int getAveLenFlower() {
        return aveLenFlower;
    }

    public void setAveLenFlower(int aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }

    @Override
    public String toString() {
        return "VisualParameters{" +
                "stemColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", aveLenFlower=" + aveLenFlower +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VisualParameters that = (VisualParameters) o;
        return aveLenFlower == that.aveLenFlower && Objects.equals(stemColour, that.stemColour) && Objects.equals(leafColour, that.leafColour);
    }

    @Override
    public int hashCode() {
        return Objects.hash(stemColour, leafColour, aveLenFlower);
    }
}
