package com.epam.rd.java.basic.task8.entity;

import java.util.Objects;

public class Flower {
    private String name;        // название цветка
    private String soil;        // грунт
    private String origin;      // страна происхождения
    private String multiplying; // размножение
    private VisualParameters visualParameters = new VisualParameters();
    private GrowingTips growingTips = new GrowingTips();

    public Flower() {

    }

    public Flower(String name, String soil, String origin, String multiplying, VisualParameters visualParameters, GrowingTips growingTips) {
        this.name = name;
        this.soil = soil;
        this.origin = origin;
        this.multiplying = multiplying;
        this.visualParameters = visualParameters;
        this.growingTips = growingTips;
    }


    public String getName() {
        return name;
    }

    public String getSoil() {
        return soil;
    }

    public String getOrigin() {
        return origin;
    }

    public String getMultiplying() {
        return multiplying;
    }

    public VisualParameters getVisualParameters() {
        return visualParameters;
    }

    public GrowingTips getGrowingTips() {
        return growingTips;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSoil(String soil) {
        this.soil = soil;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }

    public void setVisualParameters(VisualParameters visualParameters) {
        this.visualParameters = visualParameters;
    }

    public void setGrowingTips(GrowingTips growingTips) {
        this.growingTips = growingTips;
    }

    @Override
    public String toString() {
        return "Flower{" +
                "name='" + name + '\'' +
                ", soil='" + soil + '\'' +
                ", origin='" + origin + '\'' +
                ", multiplying='" + multiplying + '\'' +
                ", visualParameters=" + visualParameters +
                ", growingTips=" + growingTips +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flower flower = (Flower) o;
        return Objects.equals(name, flower.name) && Objects.equals(soil, flower.soil) && Objects.equals(origin, flower.origin) && Objects.equals(multiplying, flower.multiplying);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, soil, origin, multiplying);
    }
}
