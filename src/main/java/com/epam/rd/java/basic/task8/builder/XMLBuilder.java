package com.epam.rd.java.basic.task8.builder;

import com.epam.rd.java.basic.task8.container.Flowers;

public abstract class XMLBuilder {
    protected Flowers flowers;
    protected String outputXmlFile;

    public XMLBuilder(Flowers flowers, String outputXmlFile) {
        this.flowers = flowers;
        this.outputXmlFile = outputXmlFile;
    }

    public abstract void buildXML();
}
