package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.container.Flowers;
import com.epam.rd.java.basic.task8.entity.Flower;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.logging.Logger;

import static com.epam.rd.java.basic.task8.tags.FlowerTags.*;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

    private static final Logger LOGGER=Logger.getLogger(STAXController.class.getName());
    private String xmlFileName;
    private final XMLInputFactory factory;
    private XMLEventReader reader;
    private Flowers flowers;
    private Flower current;

    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
        flowers = new Flowers();
        factory = XMLInputFactory.newInstance();
        factory.setProperty(XMLInputFactory.SUPPORT_DTD, false);
        factory.setProperty("javax.xml.stream.isSupportingExternalEntities", false);
    }

    public void parse() {
        current = null;
        try {
            FileInputStream file = new FileInputStream(xmlFileName);
            reader = factory.createXMLEventReader(file);

            while (reader.hasNext()) {
                XMLEvent event = reader.nextEvent();
                if (event.isStartElement()) {
                    StartElement startElement = event.asStartElement();
                    event = makeStart(event, startElement);
                }
                if (event.isEndElement()) {
                    EndElement endElement = event.asEndElement();
                    if (FLOWER.getValue().equals(endElement.getName().getLocalPart())) {
                        flowers.add(current);
                    }
                }
            }
        } catch (XMLStreamException | FileNotFoundException e) {
            LOGGER.severe(e.getMessage());
        }
    }

    private XMLEvent makeStart(XMLEvent event, StartElement startElement) throws XMLStreamException {
        if (FLOWER.getValue().equals(startElement.getName().getLocalPart())) {
            current = new Flower();
        } else if (NAME.getValue().equals(startElement.getName().getLocalPart())) {
            event = reader.nextEvent();
            current.setName(event.asCharacters().getData());
        } else if (SOIL.getValue().equals(startElement.getName().getLocalPart())) {
            event = reader.nextEvent();
            current.setSoil(event.asCharacters().getData());
        } else if (ORIGIN.getValue().equals(startElement.getName().getLocalPart())) {
            event = reader.nextEvent();
            current.setOrigin(event.asCharacters().getData());
        } else if (MULTIPLYING.getValue().equals(startElement.getName().getLocalPart())) {
            event = reader.nextEvent();
            current.setMultiplying(event.asCharacters().getData());
        } else if (startElement.isStartElement()) {
            StartElement startElement1 = event.asStartElement();
            buildGrowingTips(startElement1, current);
            buildVisualParameters(startElement1, current);
        }
        return event;
    }


    private void buildVisualParameters(StartElement startElement, Flower current) throws XMLStreamException {
        XMLEvent event;
        if (STEMCOLOUR.getValue().equals(startElement.getName().getLocalPart())) {
            event = reader.nextEvent();
            current.getVisualParameters().setStemColour(event.asCharacters().getData());
        } else if (LEAFCOLOUR.getValue().equals(startElement.getName().getLocalPart())) {
            event = reader.nextEvent();
            current.getVisualParameters().setLeafColour(event.asCharacters().getData());
        } else if (AVELENFLOWER.getValue().equals(startElement.getName().getLocalPart())) {
            event = reader.nextEvent();
            current.getVisualParameters().setAveLenFlower(Integer.parseInt(event.asCharacters().getData()));
        }
    }

    private void buildGrowingTips(StartElement startElement, Flower current) throws XMLStreamException {
        XMLEvent event;
        if (TEMPERATURE.getValue().equals(startElement.getName().getLocalPart())) {
            event = reader.nextEvent();
            current.getGrowingTips().setTempreture(Integer.parseInt(event.asCharacters().getData()));
        } else if (LIGHTING.getValue().equals(startElement.getName().getLocalPart())) {
            event = reader.nextEvent();
            //current.getGrowingTips().setLighting(event.asCharacters().getData());
        } else if (WATERING.getValue().equals(startElement.getName().getLocalPart())) {
            event = reader.nextEvent();
            current.getGrowingTips().setWatering(Integer.parseInt(event.asCharacters().getData()));
        }
    }

    public Flowers getFlowers() {
        return flowers;
    }
}