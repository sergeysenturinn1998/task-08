package com.epam.rd.java.basic.task8.builder;

import com.epam.rd.java.basic.task8.container.Flowers;
import com.epam.rd.java.basic.task8.entity.Flower;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.*;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.logging.Logger;

import static com.epam.rd.java.basic.task8.tags.FlowerTags.*;

public class StAXBuilder extends XMLBuilder {
    private static final Logger LOGGER = Logger.getLogger(StAXBuilder.class.getName());
    private XMLOutputFactory factory;

    public StAXBuilder(Flowers flowers, String outputXmlFile) {
        super(flowers, outputXmlFile);
        factory = XMLOutputFactory.newInstance();
    }

    @Override
    public void buildXML() {
        try{
            XMLEventWriter writer= factory.createXMLEventWriter(new FileOutputStream(outputXmlFile));
            XMLEventFactory eventFactory = XMLEventFactory.newInstance();
            StartDocument doc=eventFactory.createStartDocument("UTF-8","1.0");
            writer.add(doc);

            Attribute attribute1=eventFactory.createAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
            Attribute attribute2=eventFactory.createAttribute(" xmlns","http://www.nure.ua");
            Attribute attribute3=eventFactory.createAttribute("xsi:schemaLocation","http://www.nure.ua input.xsd");

            StartElement startElement = eventFactory.createStartElement("","", FLOWERS.getValue());
            writer.add(startElement);
            writer.add(attribute1);
            writer.add(attribute2);
            writer.add(attribute3);

            for (Flower flower: flowers){
                makeFlower(flower,eventFactory,writer);
            }

            EndElement endElement = eventFactory.createEndElement("","", FLOWERS.getValue());
            writer.add(endElement);
            EndDocument endDocument = eventFactory.createEndDocument();
            writer.add(endDocument);

        }catch (XMLStreamException | FileNotFoundException e){
            LOGGER.severe(e.getMessage());
        }
    }

    private void makeFlower(Flower flower, XMLEventFactory factory, XMLEventWriter writer) throws XMLStreamException {
        StartElement flowerStart = factory.createStartElement("", "", FLOWER.getValue());
        EndElement flowerEnd = factory.createEndElement("", "", FLOWER.getValue());
        writer.add(flowerStart);

        makeNode(factory, writer, flower.getName(), NAME.getValue());
        makeNode(factory, writer, flower.getSoil(), SOIL.getValue());
        makeNode(factory, writer, flower.getOrigin(), ORIGIN.getValue());

        StartElement vp = factory.createStartElement("", "", VISUALPARAMETERS.getValue());
        writer.add(vp);
        makeNode(factory, writer, flower.getVisualParameters().getStemColour(), STEMCOLOUR.getValue());
        makeNode(factory, writer, flower.getVisualParameters().getLeafColour(), LEAFCOLOUR.getValue());
        makeNode(factory, writer, Integer.toString(flower.getVisualParameters().getAveLenFlower()), AVELENFLOWER.getValue());
        EndElement vpEnd = factory.createEndElement("", "", VISUALPARAMETERS.getValue());
        writer.add(vpEnd);

        StartElement grTipsStart = factory.createStartElement("", "", GROWINGTIPS.getValue());
        writer.add(grTipsStart);
        makeNode(factory, writer, Integer.toString(flower.getGrowingTips().getTempreture()), TEMPERATURE.getValue());
        makeNode(factory, writer, flower.getGrowingTips().getLighting(), LIGHTING.getValue());
        makeNode(factory, writer, Integer.toString(flower.getGrowingTips().getWatering()), WATERING.getValue());
        EndElement grTipsEnd = factory.createEndElement("", "", GROWINGTIPS.getValue());
        writer.add(grTipsEnd);

        makeNode(factory, writer, flower.getMultiplying(), MULTIPLYING.getValue());

        writer.add(flowerEnd);


    }

    private void makeNode(XMLEventFactory factory, XMLEventWriter writer, String value, String tag) throws XMLStreamException {
        StartElement name = factory.createStartElement("", "", tag);
        Characters ch = factory.createCharacters(value);
        EndElement end = factory.createEndElement("", "", tag);
        Attribute at;
        if (name.getName().getLocalPart().equals(AVELENFLOWER.getValue())){
            at = factory.createAttribute("measure", "cm");
            writer.add(name);
            writer.add(at);
            writer.add(ch);
            writer.add(end);
        }else if (name.getName().getLocalPart().equals(TEMPERATURE.getValue())){
            at = factory.createAttribute("measure", "celcius");
            writer.add(name);
            writer.add(at);
            writer.add(ch);
            writer.add(end);
        } else if (name.getName().getLocalPart().equals(LIGHTING.getValue())){
            at = factory.createAttribute("lightRequiring", "yes");
            writer.add(name);
            writer.add(at);
            //writer.add(ch);
            writer.add(end);
        } else if (name.getName().getLocalPart().equals(WATERING.getValue())){
            at = factory.createAttribute("measure", "mlPerWeek");
            writer.add(name);
            writer.add(at);
            writer.add(ch);
            writer.add(end);
        } else {
            writer.add(name);
            writer.add(ch);
            writer.add(end);
        }
    }
}
