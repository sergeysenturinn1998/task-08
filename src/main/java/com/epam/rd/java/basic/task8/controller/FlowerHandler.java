package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.container.Flowers;
import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.tags.FlowerTags;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.EnumSet;
import java.util.Locale;

import static com.epam.rd.java.basic.task8.tags.FlowerTags.*;

public class FlowerHandler extends DefaultHandler {
    private final Flowers flowers;
    private Flower current;
    private FlowerTags currentTag;
    private final EnumSet<FlowerTags> set;
    private static final String FLOWER_ELEMENT = FLOWER.getValue();

    public FlowerHandler() {
        flowers = new Flowers();
        set = EnumSet.range(NAME, MULTIPLYING);
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equals(FLOWER_ELEMENT)) {
            current = new Flower();
            return;
        }
        FlowerTags flowerTags = FlowerTags.valueOf(qName.toUpperCase(Locale.ROOT));
        if (set.contains(flowerTags)) {
            currentTag = flowerTags;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String data = new String(ch, start, length);
        if (currentTag != null) {
            switch (currentTag) {
                case NAME:
                    current.setName(data);
                    break;
                case SOIL:
                    current.setSoil(data);
                    break;
                case ORIGIN:
                    current.setOrigin(data);
                    break;
                case STEMCOLOUR:
                    current.getVisualParameters().setStemColour(data);
                    break;
                case LEAFCOLOUR:
                    current.getVisualParameters().setLeafColour(data);
                    break;
                case AVELENFLOWER:
                    current.getVisualParameters().setAveLenFlower(Integer.parseInt(data));
                    break;
                case TEMPERATURE:
                    current.getGrowingTips().setTempreture(Integer.parseInt(data));
                    break;
                case LIGHTING:
                    current.getGrowingTips().setLighting(data);
                    break;
                case WATERING:
                    current.getGrowingTips().setWatering(Integer.parseInt(data));
                    break;
                case MULTIPLYING:
                    current.setMultiplying(data);
                    break;
                default:
                    throw new EnumConstantNotPresentException(currentTag.getDeclaringClass(), currentTag.name());
            }
        }
        currentTag = null;
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equals(FLOWER_ELEMENT)){
            flowers.add(current);
        }
    }

    public Flowers getFlowers() {
        return flowers;
    }
}
