package com.epam.rd.java.basic.task8.builder;

import com.epam.rd.java.basic.task8.container.Flowers;
import com.epam.rd.java.basic.task8.entity.Flower;

import javax.sql.rowset.spi.XmlWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.logging.Logger;

import static com.epam.rd.java.basic.task8.tags.FlowerTags.*;

public class SAXBuilder extends XMLBuilder {
    private static final Logger LOGGER = Logger.getLogger(SAXBuilder.class.getName());
    private XMLOutputFactory factory;

    public SAXBuilder(Flowers flowers, String outputXmlFile) {
        super(flowers, outputXmlFile);
        factory = XMLOutputFactory.newInstance();
    }

    @Override
    public void buildXML() {
        try {
            XMLStreamWriter writer = factory.createXMLStreamWriter(new FileOutputStream(outputXmlFile));
            writer.writeStartDocument("UTF-8", "1.0");
            writer.writeStartElement(FLOWERS.getValue());
            writer.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            writer.writeAttribute("xmlns", "http://www.nure.ua");
            writer.writeAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd");

            for (Flower flower : flowers) {
                writer.writeStartElement(FLOWER.getValue());
                    makeNode(writer, flower.getName(), NAME.getValue());
                    makeNode(writer, flower.getSoil(), SOIL.getValue());
                    makeNode(writer, flower.getOrigin(), ORIGIN.getValue());



                writer.writeStartElement(VISUALPARAMETERS.getValue());
                    makeNode(writer, flower.getVisualParameters().getStemColour(), STEMCOLOUR.getValue());
                    makeNode(writer, flower.getVisualParameters().getLeafColour(), LEAFCOLOUR.getValue());
                        // with attributes
                        writer.writeStartElement(AVELENFLOWER.getValue());
                        writer.writeAttribute("measure", "cm");
                        writer.writeCharacters(Integer.toString(flower.getVisualParameters().getAveLenFlower()));
                        writer.writeEndElement();
                        //
                    writer.writeEndElement();

                writer.writeStartElement(GROWINGTIPS.getValue());
                        // with attributes
                        writer.writeStartElement(TEMPERATURE.getValue());
                        writer.writeAttribute("measure","celcius");
                        writer.writeCharacters(Integer.toString(flower.getGrowingTips().getTempreture()));
                        writer.writeEndElement();
                        //
                        //with attributes
                        writer.writeStartElement(LIGHTING.getValue());
                        //writer.writeEmptyElement(flower.getGrowingTips().getLighting());
                        writer.writeAttribute("lightRequiring","yes");
                        writer.writeEndElement();
                        //
                        // with attributes
                        writer.writeStartElement(WATERING.getValue());
                        writer.writeAttribute("measure","mlPerWeek");
                        writer.writeCharacters(Integer.toString(flower.getGrowingTips().getWatering()));
                        writer.writeEndElement();
                        //

                writer.writeEndElement();

                makeNode(writer, flower.getMultiplying(), MULTIPLYING.getValue());
                writer.writeEndElement();

            }

            writer.writeEndDocument();
            writer.flush();

        } catch (XMLStreamException | FileNotFoundException e) {
            LOGGER.severe(e.getMessage());
        }

    }

    private void makeNode(XMLStreamWriter wr, String value, String tag) throws XMLStreamException {
        wr.writeStartElement(tag);
        wr.writeCharacters(value);
        wr.writeEndElement();
    }
}
