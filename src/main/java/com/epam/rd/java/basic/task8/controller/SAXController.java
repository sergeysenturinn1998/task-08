package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.container.Flowers;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

    private static final Logger LOGGER = Logger.getLogger(SAXController.class.getName());
    private String xmlFileName;
    private Flowers flowers;
    private FlowerHandler handler;
    private XMLReader reader;


    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
        flowers = new Flowers();
        handler = new FlowerHandler();

        SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            factory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            SAXParser parser = factory.newSAXParser();
            reader = parser.getXMLReader();

        } catch (SAXException | ParserConfigurationException e) {
            LOGGER.severe(e.getMessage());
        }
        reader.setContentHandler(handler);
    }

    public void parse() {
        try {
            reader.parse(xmlFileName);
        } catch (IOException | SAXException e) {
            LOGGER.severe(e.getMessage());
        }
        flowers = handler.getFlowers();
    }

    public Flowers getFlowers() {
        return flowers;
    }
}